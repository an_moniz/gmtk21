﻿using UnityEngine;

public class BoundsPreviewer : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        Bounds b = MapBoundsCalculator.GetBounds(transform);

        Gizmos.color = Color.white;
        Vector3 tl = new Vector3(b.min.x, b.max.y, 0f);
        Vector3 tr = new Vector3(b.max.x, b.max.y, 0f);
        Vector3 bl = new Vector3(b.min.x, b.min.y, 0f);
        Vector3 br = new Vector3(b.max.x, b.min.y, 0f);

        Gizmos.DrawLine(tl, tr);
        Gizmos.DrawLine(tr, br);
        Gizmos.DrawLine(br, bl);
        Gizmos.DrawLine(bl, tl);
    }
}