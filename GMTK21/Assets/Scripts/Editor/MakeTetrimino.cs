﻿using UnityEditor;
using UnityEngine;

public class MakeTetrimino
{
    [MenuItem("GameObject/Combine Tetrimino #g", false)]
    static void MenuCombineTetrimino()
    {
        Transform[] transforms = Selection.GetTransforms(SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);

        GameObject newParent = new GameObject("Tetrimino");
        Transform newParentTransform = newParent.transform;
        Undo.RegisterCreatedObjectUndo((Object)newParent, "Create Tetrimino");

        if (transforms.Length == 1)
        {
            Transform originalParent = transforms[0].parent;
            Undo.SetTransformParent(transforms[0], newParentTransform, "Reparent Tetrimino");
            if (originalParent != null)
            {
                Undo.SetTransformParent(newParentTransform, originalParent, "Reparent Tetrimino");
            }
        }
        else if (transforms.Length > 1)
        {
            Transform originalParent = transforms[0].parent;
            Undo.SetTransformParent(transforms[0], newParentTransform, "Reparent Tetrimino");
            if (originalParent != null)
            {
                Undo.SetTransformParent(newParentTransform, originalParent, "Reparent Tetrimino");
            }

            foreach (Transform transform in transforms)
            {
                Undo.SetTransformParent(transform, newParentTransform, "Reparent Tetrimino");
            }
        }

        Rigidbody2D rb2d = newParentTransform.gameObject.AddComponent<Rigidbody2D>();
        rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
        rb2d.mass = 1000f;

        ColliderController[] blocks = newParent.GetComponentsInChildren<ColliderController>();
        BlockMovementController blockMovementController = newParentTransform.gameObject.AddComponent<BlockMovementController>();
        blockMovementController.FillBlockList(blocks);
    }

    [MenuItem("GameObject/Break Tetrimino %g", false)]
    static void BreakTetrimino()
    {
        Transform[] transforms = Selection.GetTransforms(SelectionMode.TopLevel | SelectionMode.OnlyUserModifiable);
        if (transforms.Length == 1)
        {
            Transform parent = transforms[0];
            Transform parentsParent = parent.parent;
            Transform[] children = parent.GetComponentsInChildren<Transform>();
            foreach(var child in children)
            {
                Undo.SetTransformParent(child, parentsParent, "Break Tetrimino");
            }
            Undo.DestroyObjectImmediate(transforms[0].gameObject);
        }
    }
}