﻿using UnityEngine;

public class MagnetShootController : MonoBehaviour
{
    [SerializeField] private float _maxMagnetReach = 20f;
    [SerializeField] private float _proximityInvalidatorRadius = 0.15f;
    [SerializeField] private Camera _camera;

    [Header("LineOfSight")]
    [SerializeField] private bool _useLineOfSight = false;
    [SerializeField] private float _shooterHeight = 0.8f;

    [Header("Magnets!")]
    [SerializeField] private float _magnetVelocity = 2f;
    [SerializeField] private MoveUntilCollider _magnetA;
    [SerializeField] private MoveUntilCollider _magnetB;

    BlockMagnetizer _blockMagnetizer;

    private int _defaultLayerMask = 0;

    public enum Direction
    {
        Horizontal,
        Vertical
    }

    private void Awake()
    {
        _defaultLayerMask = LayerMask.GetMask("Default");
    }

    private void OnEnable()
    {
        _camera = Camera.main;
        _blockMagnetizer = new BlockMagnetizer();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            ShootMagnet(GetMousePosition(), Direction.Horizontal);

        if (Input.GetMouseButtonDown(1))
            ShootMagnet(GetMousePosition(), Direction.Vertical);
    }

    private Vector3 GetMousePosition()
    {
        Vector3 mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0.0f;
        return mousePos;
    }

    public void ShootMagnet(Vector3 position, MagnetShootController.Direction direction)
    {
        if (_useLineOfSight)
        {
            Vector3 shooterPos = transform.position;
            Vector3 shooterHeadPos = shooterPos + Vector3.up * _shooterHeight;

            Vector3 shooterToMouse = position - shooterPos;
            Vector3 headToMouse = position - shooterHeadPos;

            if (TryRaycast(shooterPos, shooterToMouse.normalized, shooterToMouse.magnitude) &&
                TryRaycast(shooterHeadPos, headToMouse.normalized, headToMouse.magnitude))
            {
                return;
            }
        }

        Vector3 firstRayCast;
        Vector3 secondRayCast;

        if (direction == Direction.Horizontal)
        {
            firstRayCast = Vector3.left;
            secondRayCast = Vector3.right;
        }
        else
        {
            firstRayCast = Vector3.up;
            secondRayCast = Vector3.down;
        }

        if (IsMousePosValid(position))
        {
            if (_magnetA != null)
            {
                MoveUntilCollider magnetA = Instantiate<MoveUntilCollider>(_magnetA, position, Quaternion.identity);
                magnetA.Velocity = firstRayCast * _magnetVelocity;
            }

            if (_magnetB != null)
            {
                MoveUntilCollider magnetB = Instantiate<MoveUntilCollider>(_magnetB, position, Quaternion.identity);
                magnetB.Velocity = secondRayCast * _magnetVelocity;
            }

            if (TryRaycast(position, firstRayCast, _maxMagnetReach, out BlockMovementController blockA) &&
                TryRaycast(position, secondRayCast, _maxMagnetReach, out BlockMovementController blockB))
            {
                // Magnetize Go!
                if (direction == Direction.Horizontal)
                    _blockMagnetizer.PullHorizontal(blockA, blockB);
                else
                    _blockMagnetizer.PullVertical(blockA, blockB);

                if (AudioManager.instance != null)
                    AudioManager.instance.playMagnetSFX(); //play sound effect
            }
        }
    }

    private bool TryRaycast(Vector3 origin, Vector3 direction, float distance, out BlockMovementController block)
    {
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, _defaultLayerMask);
        if (hit.collider != null && hit.collider.attachedRigidbody != null)
        {
            block = hit.collider.attachedRigidbody.GetComponent<BlockMovementController>();
        }
        else
        {
            block = null;
        }
        return hit.transform != null;
    }

    private bool TryRaycast(Vector3 origin, Vector3 direction, float distance)
    {
        return TryRaycast(origin, direction, distance, out BlockMovementController block);
    }

    private bool IsMousePosValid(Vector3 mousePos)
    {
        return Physics2D.OverlapCircle(mousePos, _proximityInvalidatorRadius) == null;
    }

    private void OnDrawGizmos()
    {
        if (_camera == null) return;

        Vector3 mousePos = GetMousePosition();

        if (_useLineOfSight)
        {
            Vector3 shooterPos = transform.position;
            Vector3 shooterHeadPos = shooterPos + Vector3.up * _shooterHeight;

            Vector3 shooterToMouse = mousePos - shooterPos;
            Vector3 headToMouse = mousePos - shooterHeadPos;

            Gizmos.color = TryRaycast(shooterPos, shooterToMouse.normalized, shooterToMouse.magnitude) ? Color.red : Color.green;
            Gizmos.DrawLine(shooterPos, mousePos);

            Gizmos.color = TryRaycast(shooterHeadPos, headToMouse.normalized, headToMouse.magnitude) ? Color.red : Color.green;
            Gizmos.DrawLine(shooterHeadPos, mousePos);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 firstRayCast = Vector3.left;
            Vector3 secondRayCast = Vector3.right;

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(mousePos, _proximityInvalidatorRadius);

            if (TryRaycast(mousePos, firstRayCast, _maxMagnetReach) && IsMousePosValid(mousePos))
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;
            Gizmos.DrawLine(mousePos, mousePos + firstRayCast * _maxMagnetReach);

            if (TryRaycast(mousePos, secondRayCast, _maxMagnetReach) && IsMousePosValid(mousePos))
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;
            Gizmos.DrawLine(mousePos, mousePos + secondRayCast * _maxMagnetReach);

            Gizmos.DrawSphere(mousePos, .1f);
        }
        else if (Input.GetMouseButton(1))
        {
            Vector3 firstRayCast = Vector3.up;
            Vector3 secondRayCast = Vector3.down;

            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(mousePos, _proximityInvalidatorRadius);

            if (TryRaycast(mousePos, firstRayCast, _maxMagnetReach))
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;
            Gizmos.DrawLine(mousePos, mousePos + firstRayCast * _maxMagnetReach);

            if (TryRaycast(mousePos, secondRayCast, _maxMagnetReach))
                Gizmos.color = Color.green;
            else
                Gizmos.color = Color.red;
            Gizmos.DrawLine(mousePos, mousePos + secondRayCast * _maxMagnetReach);

            Gizmos.DrawSphere(mousePos, .1f);
        }
    }
}