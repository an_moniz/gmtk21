﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class MapGrid
{
    private bool[,] _terrainTiles;

    private Vector3 _origin;

    private float _tileSize;

    public Node VictoryNode { get; private set; }

    private Node[,] grid;
    public Node[,] Grid { get { return grid; } }

    public MapGrid( Vector3 origin, float tileSize, bool[,] terrainTiles, Node victoryNode )
    {
        CreateNodes(terrainTiles);
        _origin = origin;
        _tileSize = tileSize;
        VictoryNode = victoryNode;
    }

    void CreateNodes(bool[,] terrainTiles)
    {
        int xMax = terrainTiles.GetLength(0);
        int yMax = terrainTiles.GetLength(1);

        grid = new Node[xMax, yMax];
        for (int x = 0; x < xMax; x++)
        {
            for (int y = 0; y < yMax; y++)
                grid[x, y] = new Node(terrainTiles[x,y], GetWorldPositionFromNode(x, y), x, y);
        }
    }

    public bool IsTerrainWalkable(float x, float y )
    {
        if (x < 0 || y < 0)
            return false;
        return grid[(int)x, (int)y].walkable;
    }


    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int i = -1; i <= 1; i += 2)
        {
            int checkX = node.gridX + i;
            int checkY = node.gridY + i;
            if (IsTerrainWalkable(checkX, node.gridY))
                neighbours.Add(grid[checkX, node.gridY]);
            if (IsTerrainWalkable(node.gridX, checkY))
                neighbours.Add(grid[node.gridX, checkY]);
        }
        return neighbours;
    }

    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        int cellX = Mathf.RoundToInt(worldPosition.x / _tileSize);
        int cellY = Mathf.RoundToInt(worldPosition.y / _tileSize);
        return grid[cellX, cellY];
    }

    public Vector3 GetWorldPositionFromNode( int x, int y )
    {
        Vector3 worldPosition = new Vector3(0.0f, 0.0f, 0.0f);

        worldPosition.x = x + _origin.x;
        worldPosition.y = y + _origin.y;

        return worldPosition;
    }

    public List<Node> path;
}
