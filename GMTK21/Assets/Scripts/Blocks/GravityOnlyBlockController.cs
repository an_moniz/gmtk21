﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityOnlyBlockController : BlockMovementController
{
    public BlockMovementController _lastBlockTouched;
    private List<Collider2D> _touchingColliders = new List<Collider2D>();
    private static int _defaultMask = 0;
    private Coroutine _waitToStartCoroutine;
    private bool _waitingToStart;

    public override void Start()
    {
        base.Start();
        _defaultMask = LayerMask.GetMask("Default");
        ActivateGravity();
    }

    private void OnEnable()
    {
        _waitingToStart = false;
        if (_blocks == null || _blocks.Count == 0)
            _blocks = new List<ColliderController>(GetComponentsInChildren<ColliderController>());
    }

    public override void Revert()
    {
        _currentState = MagnetState.Default;
        _body.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        _body.gravityScale = 0;
        CancelStartGravityCoroutine();
    }

    public override void Move(int horz, int vert, BlockMovementController target)
    {

    }

    public override void Stick()
    {

    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        bool newAddition = false;
        if (collision.collider != null && collision.collider.attachedRigidbody != null && collision.collider.attachedRigidbody != _body)
        {
            newAddition = true;
            _touchingColliders.Add(collision.collider);
        }

        if (newAddition && _currentState == MagnetState.Moving  && _lastBlockTouched != collision.gameObject.GetComponent<BlockMovementController>())
        {
            DisactivateGravity();
            _lastBlockTouched = collision.gameObject.GetComponent<BlockMovementController>();
        }
    }

    private void FixedUpdate()
    {
        if (_currentState == MagnetState.Moving && _body.velocity.sqrMagnitude == 0)
        {
            Revert();
        }
    }

    private void ActivateGravity()
    {
        CancelStartGravityCoroutine();
        _waitingToStart = true;
        _waitToStartCoroutine = StartCoroutine(StartGravity());
    }

    private void CancelStartGravityCoroutine()
    {
        _waitingToStart = false;
        if (_waitToStartCoroutine != null)
        {
            StopCoroutine(_waitToStartCoroutine);
        }
    }

    private void DisactivateGravity()
    {
        CancelStartGravityCoroutine();
        _currentState = MagnetState.Default;
        _body.gravityScale = 0;
        _body.simulated = false;
        transform.position = new Vector2(Mathf.RoundToInt(transform.position.x + .1f), Mathf.RoundToInt(transform.position.y + .1f));
        _body.simulated = true;
        foreach (ColliderController block in _blocks)
        {
            block.SwitchHorizontal(true);
        }
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider != null && collision.collider.attachedRigidbody != null && collision.collider.attachedRigidbody != _body)
        {
            _touchingColliders.Remove(collision.collider);
        }

        _lastBlockTouched = collision.gameObject.GetComponent<BlockMovementController>();
        if (_touchingColliders.Count == 0)
        {
            ActivateGravity();
        }
    }

    IEnumerator StartGravity()
    {
        yield return new WaitForSeconds(.33f);
        if (_waitingToStart)
        {
            _body.gravityScale = 1;
            yield return new WaitForFixedUpdate(); // Hack to ensure FixedUpdate waits for gravityScale to affect gravity before checking if it was stopped by something
            _currentState = MagnetState.Moving;
            foreach (ColliderController block in _blocks)
            {
                block.SwitchHorizontal(false);
            }
        }
        _waitingToStart = false;
    }
}
