﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMagnetizer
{
    BlockMovementController[] _lastBlocks = new BlockMovementController[2];


    public void PullHorizontal(BlockMovementController leftBlock, BlockMovementController rightBlock)
    {
        if (leftBlock == null || rightBlock == null)
            return;

        if (leftBlock._currentState == MagnetState.Moving || rightBlock._currentState == MagnetState.Moving)
            return;

        if (_lastBlocks[0] != null && _lastBlocks[0] != leftBlock && _lastBlocks[0] != rightBlock)
            _lastBlocks[0].Revert();

        if (_lastBlocks[1] != null && _lastBlocks[1] != leftBlock && _lastBlocks[1] != rightBlock)
            _lastBlocks[1].Revert();

        leftBlock.Move(1, 0, rightBlock);
        rightBlock.Move(-1, 0, leftBlock);
        _lastBlocks[0] = leftBlock;
        _lastBlocks[1] = rightBlock;
    }

    public void PullVertical(BlockMovementController topBlock, BlockMovementController bottomBlock)
    {
        if (bottomBlock == null || topBlock == null)
            return;

        if (bottomBlock._currentState == MagnetState.Moving || topBlock._currentState == MagnetState.Moving)
            return;

        if (_lastBlocks[0] != null && _lastBlocks[0] != bottomBlock && _lastBlocks[0] != topBlock)
            _lastBlocks[0].Revert();

        if (_lastBlocks[1] != null && _lastBlocks[1] != bottomBlock && _lastBlocks[1] != topBlock)
            _lastBlocks[1].Revert();

        bottomBlock.Move(0, 1, topBlock);
        topBlock.Move(0, -1, bottomBlock);
        _lastBlocks[0] = bottomBlock;
        _lastBlocks[1] = topBlock;
    }
}
