﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    [SerializeField] private float _lifetime = 1f;
    private float _timeAlive = 0;

    void Update()
    {
        _timeAlive += Time.deltaTime;
        if (_timeAlive > _lifetime)
            Destroy(gameObject);
    }
}
