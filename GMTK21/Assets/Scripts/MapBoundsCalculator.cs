﻿using UnityEngine;

public static class MapBoundsCalculator
{
    public static Bounds GetBounds( Transform root )
    {
        Bounds bounds = new Bounds(root.position, Vector3.one);
        Renderer[] renderers = root.gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            bounds.Encapsulate(renderer.bounds);
        }
        return bounds;
    }
}