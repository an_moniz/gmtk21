﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    //audio
    [SerializeField]
    public AudioClip death;
    public AudioClip victory;
    public AudioClip magnet;
    AudioSource audioSource;

    public static AudioManager instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playDeathSFX()
    {
        audioSource.PlayOneShot(death, 0.08F);
    }
    public void playVictorySFX()
    {
        audioSource.PlayOneShot(victory, 0.83F);
    }
    public void playMagnetSFX()
    {
        audioSource.PlayOneShot(magnet, 0.83F);
    }
}
