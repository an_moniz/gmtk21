﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [System.Serializable]
    public struct MapPieceInfo
    {
        public string Name;

        public Color32 Color;
        public SpriteRenderer Prefab;
        public Sprite[] Sprites;
        public bool IsOnFloorPiece;

        public Sprite GetRandomSprite()
        {
            return Sprites[Random.Range(0, Sprites.Length)];
        }
    }

    [System.Serializable]
    public struct MapTerrainInfo
    {
        public Color32 Color;
        public bool IsWalkable;
        public SpriteRenderer Prefab;
        public Sprite[] Sprites;

        public Sprite GetRandomSprite()
        {
            return Sprites[Random.Range(0, Sprites.Length)];
        }
    }

    [System.Serializable]
    public struct MapUnitInfo
    {
        public string Name;
        public Color32 Color;
        public SpriteRenderer Prefab;
    }

    public Texture2D MapData;

    public float _tileSize = 1.0f;

    public MapTerrainInfo[] _terrainPieces;
    public MapUnitInfo[] _unitPieces;

    private MapTerrainInfo _unitTerrain;

    void Awake()
    {
        for (int i = 0; i < _terrainPieces.Length; ++i)
        {
            if (_terrainPieces[i].IsWalkable)
            {
                _unitTerrain = _terrainPieces[i];
                break;
            }
        }
    }

    public void CleanMap()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            // TODO: Handle Player 'specially??
            Transform child = transform.GetChild(i);
            DestroyImmediate(child.gameObject);
        }
    }

    public MapGrid GenerateMap(Texture2D mapData)
    {
        CleanMap();

        Color32[] rawTexture = mapData.GetPixels32();

        Vector2 textureDimensions = new Vector2(mapData.width, mapData.height );

        bool[,] terrainTypes = new bool[(int)textureDimensions.x, (int)textureDimensions.y];

        Node victoryNode = null;

        for (int x = 0; x < textureDimensions.x; ++x)
        {
            for (int y = 0; y < textureDimensions.y; ++y)
            {
                Color32 textureColor = rawTexture[(int)(y * textureDimensions.x + x)];
                
                for (int i = 0; i < _terrainPieces.Length ; ++i)
                {
                    if (_terrainPieces[i].Color.Equals(textureColor) )
                    {
                        InstantiateMapPieceAt(_terrainPieces[i], x, y);
                        terrainTypes[x, y] = _terrainPieces[i].IsWalkable;
                    }
                }
                for (int i = 0; i < _unitPieces.Length; ++i)
                {
                    if (_unitPieces[i].Color.Equals(textureColor))
                    {
                        InstantiateMapPieceAt(_unitTerrain, x, y);
                        terrainTypes[x, y] = true;


                        if (_unitPieces[i].Name == "Player")
                        {
                            if (GetPlayer() == null)
                            {
                                InstantiateMapPieceAt(_unitPieces[i], x, y);
                            }
                            else
                            {
                                RepositionPlayerPieceAt(GetPlayer(), x, y);
                            }
                        }
                        else
                        {
                            GameObject newObject = InstantiateMapPieceAt(_unitPieces[i], x, y);

                            if (_unitPieces[i].Name == "Exit")
                            {
                                victoryNode = new Node(true, newObject.transform.position, x, y);
                            }
                        }
                    }
                }
            }
        }

        return new MapGrid(transform.position, _tileSize, terrainTypes, victoryNode);
    }

    private GameObject GetPlayer()
    {
        return null;
    }

    public void InstantiateMapPieceAt(MapTerrainInfo mapPiece, int x, int y)
    {
        SpriteRenderer newObject = Instantiate<SpriteRenderer>(mapPiece.Prefab);
        if (mapPiece.Sprites.Length > 0)
        {
            newObject.sprite = mapPiece.GetRandomSprite();
        }
        newObject.transform.parent = transform;
        newObject.transform.localPosition = new Vector3(x * _tileSize, y * _tileSize, 0.0f);
        newObject.transform.rotation = Quaternion.identity;
    }

    public GameObject InstantiateMapPieceAt( MapUnitInfo mapPiece, int x, int y )
    {
        SpriteRenderer newObject = Instantiate<SpriteRenderer>(mapPiece.Prefab);
        newObject.transform.parent = transform;
        newObject.transform.localPosition = new Vector3(x * _tileSize, y * _tileSize, 0.0f);
        newObject.transform.rotation = Quaternion.identity;
        return newObject.gameObject;
    }

    private void RepositionPlayerPieceAt(GameObject player, int x, int y)
    {
        player.transform.parent = transform;
        player.transform.localPosition = new Vector3(x * _tileSize, y * _tileSize, 0.0f);
        player.transform.rotation = Quaternion.identity;
    }
}
