﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderController : MonoBehaviour
{
    [SerializeField] GameObject _colliderV;
    [SerializeField] GameObject _colliderH;

    public void SwitchHorizontal(bool isOn)
    {
        _colliderH.SetActive(isOn);
    }

    public void SwitchVertical(bool isOn)
    {
        _colliderV.SetActive(isOn);
    }
}
