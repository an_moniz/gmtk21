﻿public enum GameState
{
    StartScreen,
    GameScreen,
    TransitionScreen,
    FailureScreen,
    VictoryScreen,
    CreditsScreen
}