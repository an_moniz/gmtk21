﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationaryBlockController : BlockMovementController
{
    public override void Revert()
    {
        _currentState = MagnetState.Default;
        _body.constraints = RigidbodyConstraints2D.FreezeAll;
        _body.mass = 1000f;
    }

    public override void Move(int horz, int vert, BlockMovementController target)
    {
        
    }

    public override void Stick()
    {

    }
}
