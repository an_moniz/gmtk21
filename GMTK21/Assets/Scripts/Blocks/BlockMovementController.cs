﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MagnetState
{
    Default,
    Moving,
    Sticking
}

public class BlockMovementController : MonoBehaviour
{
    [SerializeField] protected List<ColliderController> _blocks;
    protected Rigidbody2D _body;
    protected RigidbodyConstraints2D _defaultConstraints;
    public MagnetState _currentState;
    protected BlockMovementController _target;
    private ScreenShake _shakeCamera;

    // Start is called before the first frame update
    public virtual void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        _defaultConstraints = _body.constraints;
        _shakeCamera = Camera.main.GetComponent<ScreenShake>();
        Revert();
    }

    public virtual void Revert()
    {
        _body.gravityScale = 1;
        _body.mass = 1000f;
        _currentState = MagnetState.Default;
        _body.constraints = _defaultConstraints;
        foreach (ColliderController block in _blocks)
        {
            block.SwitchHorizontal(false);
        }
    }

    public void FillBlockList(ColliderController[] blocks)
    {
        _blocks = new List<ColliderController>(blocks);
    }

    public virtual void Move(int horz, int vert, BlockMovementController target)
    {
        _currentState = MagnetState.Moving;
        _target = target;
        if (horz != 0)
        {
            _body.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
            //_body.AddForce(new Vector2(980 * horz, 0));
            _body.velocity = new Vector2(20 * horz, 0);
            foreach (ColliderController block in _blocks)
            {
                block.SwitchVertical(false);
                block.SwitchHorizontal(true);
            }
        }
        else
        {
            _body.gravityScale = 0;
            _body.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            //_body.AddForce(new Vector2(0, 980 * vert));
            _body.velocity = new Vector2(0, 20 * vert);
            foreach (ColliderController block in _blocks)
            {
                block.SwitchHorizontal(false);
            }
        }
    }

    private void FixedUpdate()
    {
        if (_currentState == MagnetState.Moving && _body.velocity.sqrMagnitude == 0)
        {
            ShakeScreen();
            Stick();
        }
    }

    public virtual void Stick()
    {
        _body.simulated = false;
        transform.position = new Vector2(Mathf.RoundToInt(transform.position.x + .1f), Mathf.RoundToInt(transform.position.y + .1f));
        _currentState = MagnetState.Sticking;
        _body.constraints = RigidbodyConstraints2D.FreezeAll;
        foreach (ColliderController block in _blocks)
        {
            block.SwitchVertical(true);
        }
        _body.simulated = true;
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (_currentState == MagnetState.Moving &&
            collision.gameObject.GetComponent<BlockMovementController>())
        {
            var gravityBlock = collision.gameObject.GetComponent<GravityOnlyBlockController>();
            if (gravityBlock && gravityBlock._currentState == MagnetState.Moving)
                return;
            ShakeScreen();
            Stick();
        }

    }

    private void ShakeScreen()
    {
        if (_shakeCamera != null)
            _shakeCamera.StartShake(0.6f);
    }
}
