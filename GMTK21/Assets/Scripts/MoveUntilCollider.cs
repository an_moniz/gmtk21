﻿using UnityEngine;

public class MoveUntilCollider : MonoBehaviour
{
    private SpriteRenderer _spriteRender;
    private BoxCollider2D _collider2D;
    private int _defaultLayer;

    public Vector3 Velocity;
    [SerializeField] private GameObject _onDeathExplosion;

    private static Quaternion MagnetUpRot = Quaternion.Euler(0, 0, 90f);
    private static Quaternion MagnetDownRot = Quaternion.Euler(0, 0, 270f);

    private void Awake()
    {
        _spriteRender = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<BoxCollider2D>();
        _defaultLayer = LayerMask.GetMask("Default");
    }

    private void FixedUpdate()
    {
        transform.position += Velocity * Time.fixedDeltaTime;

        _spriteRender.flipX = Velocity.x < 0;
        if (Velocity.y > 0)
        {
            transform.rotation = MagnetUpRot;
        }
        else if (Velocity.y < 0)
        {
            transform.rotation = MagnetDownRot;
        }
        else
        {
            transform.rotation = Quaternion.identity;
        }

        Collider2D contact = Physics2D.OverlapBox(transform.position, _collider2D.bounds.size, 0, _defaultLayer);
        if (contact != null)
        {
            Instantiate(_onDeathExplosion, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEndter2D(Collision2D collision)
    {
        if (!collision.gameObject.tag.Equals("player"))
        {
            Instantiate(_onDeathExplosion, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}