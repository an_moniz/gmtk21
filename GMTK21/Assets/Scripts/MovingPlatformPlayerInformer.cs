﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformPlayerInformer : MonoBehaviour
{
    public Vector3 OldPosition { get; private set; }

    public Vector3 FixedPositionDelta => _interpolatedTransform.FixedPositionDelta;

    private InterpolatedTransform _interpolatedTransform;

    private void Awake()
    {
        _interpolatedTransform = GetComponentInParent<InterpolatedTransform>();
    }
}