﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private float _levelLoadTime;
    [SerializeField] private RectTransform _transitionScreen;

    [SerializeField] private RectTransform _startScreen;
    [SerializeField] private RectTransform _gameHUD;
    [SerializeField] private RectTransform _victoryScreen;
    [SerializeField] private RectTransform _deathScreen;
    [SerializeField] private RectTransform _creditsScreen;
    [SerializeField] private GameObject _root3d;

    [SerializeField] private GameObject[] _level;

    private GameObject _currentLevel;

    public static GameManager instance;

    public int nextLevel = 0;


    public GameState _gameState = GameState.StartScreen;
    private GameState GameState

    {
        get { return _gameState; }
        set
        {
            _gameState = value;
            _gameHUD.gameObject.SetActive(_gameState == GameState.GameScreen);
            _root3d.SetActive(_gameState == GameState.GameScreen);
            _transitionScreen.gameObject.SetActive(_gameState == GameState.TransitionScreen);
            _startScreen.gameObject.SetActive(_gameState == GameState.StartScreen);
            _victoryScreen.gameObject.SetActive(_gameState == GameState.VictoryScreen);
            _deathScreen.gameObject.SetActive(_gameState == GameState.FailureScreen);
            _creditsScreen.gameObject.SetActive(_gameState == GameState.CreditsScreen);
        }
    }

    private Camera _mainCamera;


    void Awake()
    {
        _mainCamera = Camera.main;

        _transitionScreen.gameObject.SetActive(false);
        _startScreen.gameObject.SetActive(false);
        _gameHUD.gameObject.SetActive(false);

        instance = this;
    }

    void Start()
    {
        GoToStartScreen();
    }

    void Update()
    {
        if (GameState == GameState.StartScreen)
        {
            if (Input.GetButtonDown("Jump"))
            {
                nextLevel = 0;
                ResetLevel();
            }
        }
        else if (GameState == GameState.VictoryScreen)
        {
            if (Input.anyKeyDown)
            {
                ShowCredits();
            }
        }
        else if (GameState == GameState.FailureScreen)
        {
            if (Input.anyKeyDown)
            {
                GameState = GameState.GameScreen;
                ContinueGame();
            }
        }
        else if (GameState == GameState.CreditsScreen)
        {
            if (Input.GetButtonDown("Jump"))
            {
                GoToStartScreen();
            }
        }
    }

    public void ContinueGame()
    {
        StartGame();
    }

    private void ShowFailureScreen()
    {
        GameState = GameState.FailureScreen;
    }

    private void ShowCredits()
    {
        StartCoroutine("ShowCreditsCoroutine");
    }

    private IEnumerator ShowCreditsCoroutine()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GameState = GameState.CreditsScreen;
    }

    private IEnumerator LoadVictoryScreenCoroutine()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GameState = GameState.VictoryScreen;
    }

    private IEnumerator LoadNextLevelCoroutine()
    {
        yield return new WaitForSeconds(_levelLoadTime);
        GoToNextLevel();
    }

    public void GoToStartScreen()
    {
        CleanGame();
        GameState = GameState.StartScreen;
    }

    private void CleanGame()
    {
        Destroy(_currentLevel);
    }

    public void StartGame()
    {
        GameState = GameState.GameScreen;
    }

    public void GoToNextLevel()
    {
        nextLevel++;
        if (nextLevel >= _level.Length)
        {
            StartCoroutine("LoadVictoryScreenCoroutine");
        }
        else
        {
            ResetLevel();
        }
    }

    public void ResetLevel()
    {
        CleanGame();
        _currentLevel = Instantiate(_level[nextLevel]);
        Bounds bounds = MapBoundsCalculator.GetBounds(_currentLevel.transform);
        Vector3 centerOffset = new Vector3(Mathf.Round(bounds.center.x), Mathf.Round(bounds.center.y), 0f);
        //Vector3 moveAmount = new Vector3(-Mathf.Ceil(bounds.size.x / 2f), Mathf.Ceil(bounds.size.y / 2f), 0f);
        _currentLevel.transform.position -= centerOffset;// + moveAmount;
        StartGame();
    }

}
