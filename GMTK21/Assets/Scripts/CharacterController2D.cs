﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController2D : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed = 1.2f;
    private float _currentSpeed = 0.0f;
    [SerializeField]
    private float _jumpHeight = 6.5f;

    [SerializeField] private float _footRayCastLength = 0.08f;
    [SerializeField] private float _forwardRayCastLength = 0.1f;
    [SerializeField] private float _forwardRayCastHeightPercentage = 0.8f;

    private const float _speedToMove = 0.01f;
    private int _moveDirection = 0;
    private int _lastMoveDirection = 0;
    private bool _isGrounded = false;
    private bool _jumpRequested = false;

    private Rigidbody2D _r2d;
    private BoxCollider2D _collider;
    [SerializeField] private SpriteRenderer _spriteRdr;
    [SerializeField] private Animator _animator;
    private float _initialGravityScale = 1.0f;

    [Header("CrushCollisionDetection")]
    [SerializeField] Vector2 _crushCollisionOffset = new Vector2(0, 0.5f);
    [SerializeField] Vector2 _crushCollisionSize = new Vector2(0.428f, 0.975f);
    //audio
    [Header("Audio")]
    [SerializeField]
    public AudioClip jump;
    public AudioClip land;
    AudioSource audioSource;

    [SerializeField] private bool _panningCamera = false;

    private Transform _initialParent;

    void Awake()
    {
        _r2d = GetComponent<Rigidbody2D>();
        _collider = GetComponent<BoxCollider2D>();

        _initialGravityScale = _r2d.gravityScale;

        audioSource = GetComponent<AudioSource>();
    }

    private Camera _mainCamera;
    private Bounds _screenBounds;
    private void OnEnable()
    {
        _initialParent = transform.parent;
        _mainCamera = Camera.main;
        if (_mainCamera != null)
        {
            Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
            _screenBounds = new Bounds(_mainCamera.ScreenToWorldPoint(new Vector3(0,0,0)), screenBounds);
        }
    }

    private void LateUpdate()
    {
        if (_panningCamera)
        {
            Vector2 screenBounds = Camera.main.ScreenToWorldPoint(_mainCamera.transform.localPosition + new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
            Vector3 cameraWorldPoint = _mainCamera.transform.localPosition;//.ScreenToWorldPoint(new Vector2(0.5f, 0.5f));
            _screenBounds = new Bounds(cameraWorldPoint, new Vector3(screenBounds.x, screenBounds.y, 20f));
            Bounds biggerBounds = new Bounds(cameraWorldPoint, new Vector3(screenBounds.x, screenBounds.y, 20f) * 1.2f);
            Vector3 cameraTargetPosition = _mainCamera.transform.localPosition;
            float zFuck = _mainCamera.transform.localPosition.z;
            if (!biggerBounds.Contains(_collider.bounds.center))
            {
                Vector3 vecToChar = transform.position - biggerBounds.center;
                cameraTargetPosition = (cameraTargetPosition + vecToChar) / 4f;
            }
            else 
            {
                cameraTargetPosition = Vector3.zero;
            }

            Vector3 newPosition = Vector3.Lerp(_mainCamera.transform.localPosition, cameraTargetPosition, Time.deltaTime);
            newPosition.z = zFuck;
            _mainCamera.transform.localPosition = newPosition;
        }
    }

    void Update()
    {

        float horizontalMoveInput = Input.GetAxisRaw("Horizontal");
        //float verticalMoveInput = Input.GetAxisRaw("Vertical");

        if ( (Mathf.Abs(horizontalMoveInput) > Mathf.Epsilon) )
        {
            _moveDirection = horizontalMoveInput > 0 ? 1 : -1;
            //Debug.Log("moving: " + _moveDirection);
        }
        else
        {
            if (_isGrounded || _r2d.velocity.magnitude < _speedToMove)
            {
                _moveDirection = 0;
            }
        }

        // Change Facing Direction
        if (Mathf.Abs(_moveDirection) > 0)
        {
            _spriteRdr.flipX = _moveDirection > 0;
        }

        // Jumping
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _jumpRequested = true;
            audioSource.PlayOneShot(jump, 0.7F);
        }

        _r2d.gravityScale = (_isGrounded) ? 0.0f : _initialGravityScale;
        

        // change animation bools
        _animator.SetBool("running", Mathf.Abs(_moveDirection) > 0);
        _animator.SetBool("jumping", !_isGrounded);

    }
    //reaching the exit
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.name == "door")
        {
            AudioManager.instance.playVictorySFX();
            GameManager.instance.GoToNextLevel();
        }
    }

    private bool BoxcastToPlatform()
    {
        Bounds colliderBounds = _collider.bounds;
        Vector3 groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, _footRayCastLength * 0.5f, 0.0f);
        RaycastHit2D[] raycasts = Physics2D.BoxCastAll(groundCheckPos, new Vector2(_collider.size.x * 0.5f, _footRayCastLength), 0f, Vector2.down, _footRayCastLength);

        for(int i = 0, size = raycasts.Length; i < size; i++)
        {
            if (raycasts[i].collider != _collider)
            {
                return true;
            }
        }
        return false;
    }

    private bool RaycastForward()
    {
        Vector2 direction = transform.right * (_spriteRdr.flipX ? 1 : -1);
        RaycastHit2D raycast = Physics2D.BoxCast(transform.position, new Vector2(0.001f, _collider.size.y * _forwardRayCastHeightPercentage), 
            0f, direction, _forwardRayCastLength - 0.001f, LayerMask.GetMask("Wall"));

        if (raycast.collider != null && raycast.collider != _collider)
        {
            return true;
        }
        return false;
    }

    private List<Rigidbody2D> _verticalRigidbodies = new List<Rigidbody2D>();
    private List<Rigidbody2D> _horizontalRigidbodies = new List<Rigidbody2D>();
    private void CheckForCrush()
    {
        _verticalRigidbodies.Clear();
        _horizontalRigidbodies.Clear();
        Collider2D[] colliders = Physics2D.OverlapBoxAll(new Vector2(transform.position.x, transform.position.y) + 
            Vector2.up * _crushCollisionSize.y / 2, _crushCollisionSize, 0, LayerMask.GetMask("Default"));
        Bounds charCrushBounds = new Bounds(new Vector2(transform.position.x, transform.position.y) +
            Vector2.up * _crushCollisionSize.y / 2, _crushCollisionSize);

        foreach(var collider in colliders)
        {
            if (collider.attachedRigidbody != null)
            {
                if (charCrushBounds.min.y >= collider.bounds.max.y ||
                    charCrushBounds.max.y <= collider.bounds.min.y)
                {
                    if (!_verticalRigidbodies.Find(x => collider.attachedRigidbody == x))
                    {
                        _verticalRigidbodies.Add(collider.attachedRigidbody);
                    }
                }
                else
                {
                    if (!_horizontalRigidbodies.Find(x => collider.attachedRigidbody == x))
                    {
                        _horizontalRigidbodies.Add(collider.attachedRigidbody);
                    }
                }
            }
        }

        if (_verticalRigidbodies.Count > 1 || _horizontalRigidbodies.Count > 1)
        {
            if (!DetectCrush(_verticalRigidbodies, charCrushBounds.center))
                DetectCrush(_horizontalRigidbodies, charCrushBounds.center);
        }
    }

    private bool DetectCrush( List<Rigidbody2D> rbodies, Vector2 charCenterPoint )
    {
        bool killed = false;
        if (rbodies.Count > 1)
        {
            foreach (var rigidbody in rbodies)
            {
                if (rigidbody.velocity.sqrMagnitude > 0)
                {
                    foreach (var otherRB in rbodies)
                    {
                        if (otherRB != rigidbody)
                        {
                            if (rigidbody.velocity.x > 0 && otherRB.velocity.x <= 0)
                            {
                                if (rigidbody.centerOfMass.x <= charCenterPoint.x && charCenterPoint.x <= otherRB.centerOfMass.x)
                                {
                                    // Crush
                                    Die();
                                    killed = true;
                                }
                            }
                            else if (rigidbody.velocity.x < 0 && otherRB.velocity.x >= 0)
                            {
                                if (rigidbody.centerOfMass.x >= charCenterPoint.x && charCenterPoint.x >= otherRB.centerOfMass.x)
                                {
                                    // Crush
                                    Die();
                                    killed = true;
                                }
                            }
                            else if (rigidbody.velocity.y > 0 && otherRB.velocity.y <= 0)
                            {
                                if (rigidbody.centerOfMass.y <= charCenterPoint.y && charCenterPoint.y <= otherRB.centerOfMass.y)
                                {
                                    // Crush
                                    Die();
                                    killed = true;
                                }
                            }
                            else if (rigidbody.velocity.y < 0 && otherRB.velocity.y >= 0)
                            {
                                if (rigidbody.centerOfMass.y >= charCenterPoint.y && charCenterPoint.y >= otherRB.centerOfMass.y)
                                {
                                    // Crush
                                    Die();
                                    killed = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return killed;
    }

    void FixedUpdate()
    {
        Bounds colliderBounds = _collider.bounds;
        float colliderRadius = _collider.size.x * 0.5f;
        Vector3 groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, 0.0f, 0.0f);
        // Check if player is grounded
        colliderRadius *= 2;
        Collider2D[] colliders = Physics2D.OverlapBoxAll(groundCheckPos, new Vector2(colliderRadius, _footRayCastLength), 0f, LayerMask.GetMask("Default"));

        bool onMovingPlatform = false;
        Vector2 movingPlatformVelocity = Vector2.zero;
        float yVel = _r2d.velocity.y;
        bool wasGrounded = _isGrounded;
        bool wasJumpRequested = _jumpRequested;

        if (_jumpRequested)
        {
            _isGrounded = false;
            yVel = _jumpHeight;
            _jumpRequested = false;
        }
        else
        {
            bool raycastBoxDown = BoxcastToPlatform();
            if (colliders.Length > 0 && raycastBoxDown)
            {
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i] != _collider)
                    {
                        if (!_isGrounded) audioSource.PlayOneShot(land, 0.7F);
                        _isGrounded = true;
                        //if (Mathf.Abs(yVel) < Mathf.Epsilon )
                        if (colliders[i].attachedRigidbody != null)
                        {
                            onMovingPlatform = true;
                            transform.parent = colliders[i].attachedRigidbody.transform;
                            movingPlatformVelocity = colliders[i].attachedRigidbody.velocity;
                            //transform.position = transform.position + Vector3.up * (pController.FixedPositionDelta.y);
                        }
                        break;
                    }
                }
            } 
            else
            {
                _isGrounded = false;
            }
        }
        if (!onMovingPlatform)
        {
            transform.parent = _initialParent;
        }

        if (yVel > 0.5f && !wasJumpRequested && wasGrounded && !_isGrounded)
        {
            //You got launched (probably)
            yVel = _jumpHeight;
        }

        bool fwdBlocked = RaycastForward();
        if (fwdBlocked)
        {
            //Debug.Log("fwd BLOCKED");
            _moveDirection = 0;
        }
        // Apply Movement Velocity
        if (_isGrounded)
        {
            //Debug.Log("current speed: " + _currentSpeed + ", max speed: " + _maxSpeed);
            _currentSpeed = _maxSpeed;
        }
        else
        {
            float speedChange = 0.0f;
            if (_moveDirection == _lastMoveDirection)
            {
                speedChange += _maxSpeed * 0.1f;
            }
            else if (_moveDirection == -_moveDirection)
            {
                speedChange -= _maxSpeed * 0.1f;
            }

            _currentSpeed += speedChange;
            if (Mathf.Abs(_currentSpeed) > _maxSpeed)
            {
                _currentSpeed = _maxSpeed;
            }
        }
        float newVel = (_moveDirection) * _currentSpeed;
        _r2d.velocity = new Vector2(newVel, yVel) + movingPlatformVelocity;

        _lastMoveDirection = (newVel > Mathf.Epsilon) ? 1 : (newVel < -Mathf.Epsilon) ? -1 : 0;

        CheckForCrush();
        // player falling off level resets the level and plays fall sound
        if (transform.position.y <= -10)
        {
            Die();
        }

        // Simple debug
        Vector3 direction = transform.right * (_spriteRdr.flipX ? 1 : -1);
        Debug.DrawLine(transform.position, transform.position + direction * 0.1f, !fwdBlocked ? Color.green : Color.red);
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(0, _footRayCastLength * 0.5f, 0), _isGrounded ? Color.green : Color.red);
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(colliderRadius * 0.5f, 0, 0), _isGrounded ? Color.green : Color.red);

        RaycastHit2D[] raycasts = Physics2D.BoxCastAll(groundCheckPos, new Vector2(_collider.size.x * 0.5f, _footRayCastLength * 0.1f), 0f, Vector2.down, _footRayCastLength);
        Debug.DrawLine(groundCheckPos + new Vector3(-_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0), 
            groundCheckPos + new Vector3(-_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0f), Color.white);
        Debug.DrawLine(groundCheckPos + new Vector3(_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0), 
            groundCheckPos + new Vector3(_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0f), Color.white);
    }

    private void Die()
    {
        if (AudioManager.instance != null)
            AudioManager.instance.playDeathSFX();
        GameManager.instance.ResetLevel();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(new Vector3(transform.position.x + _crushCollisionOffset.x, transform.position.y + _crushCollisionOffset.y, 0f), _crushCollisionSize);

        if (_collider == null) return;
        // BoxOverlap
        Bounds colliderBounds = _collider.bounds;
        float colliderRadius = _collider.size.x * 0.5f;
        Vector3 groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, 0.0f, 0.0f);
        // Check if player is grounded
        colliderRadius *= 2;
        Collider2D[] colliders = Physics2D.OverlapBoxAll(groundCheckPos, new Vector2(colliderRadius, _footRayCastLength), 0f, LayerMask.GetMask("Default"));
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(groundCheckPos, 0.01f);
        Gizmos.DrawLine(groundCheckPos, groundCheckPos + Vector3.down * _footRayCastLength / 2f);

        //
        colliderBounds = _collider.bounds;
        groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, _footRayCastLength * 0.5f, 0.0f);
        colliderRadius = _collider.size.x * 0.5f;

        // Boxcast
        Gizmos.color = Color.white;
        //Gizmos.DrawSphere(groundCheckPos + new Vector3(-_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0), .05f);
        Gizmos.DrawLine(groundCheckPos + new Vector3(-_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0),
                        groundCheckPos + new Vector3(-_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0) + new Vector3(0f, -_footRayCastLength, 0f));
        Gizmos.DrawLine(groundCheckPos + new Vector3(_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0),
                        groundCheckPos + new Vector3(_collider.size.x * 0.5f, _footRayCastLength * 0.1f, 0) + new Vector3(0f, -_footRayCastLength, 0f));
    }
}