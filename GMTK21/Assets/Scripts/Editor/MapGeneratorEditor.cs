﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    private MapGenerator _generator;
    private int _levelIndexToLoad;

    void OnEnable()
    {
        _generator = (MapGenerator)target;
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_generator != null && (GUILayout.Button("Generate Level")))
        {
            _generator.CleanMap();
            _generator.GenerateMap(_generator.MapData);
        }
    }
}
