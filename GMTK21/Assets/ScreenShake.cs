﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    private Vector3 _initialPosition;
    private float _shakeDuration = 0.0f;
    [SerializeField] private float _shakeMagnitude = 0.3f;
    [SerializeField] private float _dampingSpeed = 6f;

    public void Initialize()
    {
        _initialPosition = transform.localPosition;
    }

    private void OnEnable()
    {
        Initialize();
    }

    public void StartShake( float duration )
    {
        _shakeDuration = duration;
    }

    void Update()
    {
        if (_shakeDuration > 0)
        {
            transform.localPosition = _initialPosition + Random.insideUnitSphere * _shakeMagnitude;

            _shakeDuration -= Time.deltaTime * _dampingSpeed;
        }
        else
        {
            _shakeDuration = 0f;
            transform.localPosition = _initialPosition;
        }
    }
}
