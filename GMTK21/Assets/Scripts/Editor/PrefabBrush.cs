﻿using System.Collections.Generic;
using UnityEngine;

namespace UnityEditor
{
    [CreateAssetMenu(fileName = "Prefab Brush", menuName = "Brushes/Prefab Brush")]
    [CustomGridBrush(false, true, false, "Prefab Brush")]
    public class PrefabBrush : UnityEditor.Tilemaps.GridBrush
    {
        public List<GameObject> Prefabs;
        public float PerlinScale = 0.5f;
        private const float k_PerlinOffset = 100000f;
        public int Z;
        private GameObject _prevBrushTarget;
        private Vector3Int _prevPosition;
        public Vector2 _paintOffset;

        public int PrefabToPrint = 0;
        public bool RandomPrefab = true;

        public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
        {
            Transform itemInCell = GetObjectInCell(grid, brushTarget.transform, new Vector3Int(position.x, position.y, Z));
            if (itemInCell != null) return;
            //if (position == _prevPosition) return;
            _prevPosition = position;
            if (brushTarget != null)
                _prevBrushTarget = brushTarget;
            brushTarget = _prevBrushTarget;

            // Don't allow editing palettes
            if (brushTarget.layer == 31)
                return;

            int index;

            if (!RandomPrefab)
            {
                if (Prefabs.Count < PrefabToPrint)
                {
                    Debug.Log("No item in array of Prefabs, setting to Random");
                    // Using perlin noise to generate random index
                    index = Mathf.Clamp(Mathf.FloorToInt(GetPerlinValue(position, PerlinScale, k_PerlinOffset) * Prefabs.Count), 0, Prefabs.Count - 1);
                    RandomPrefab = true;
                }
                else
                {
                    index = PrefabToPrint;
                }
            }
            else
            {
                index = Mathf.Clamp(Mathf.FloorToInt(GetPerlinValue(position, PerlinScale, k_PerlinOffset) * Prefabs.Count), 0, Prefabs.Count - 1);
            }

            GameObject prefab = Prefabs[index];
            GameObject instance = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
            if (instance != null)
            {
                Undo.MoveGameObjectToScene(instance, brushTarget.scene, "Paint prefabs");
                Undo.RegisterCreatedObjectUndo((Object)instance, "Paint Prefabs");
                instance.transform.SetParent(brushTarget.transform);
                instance.transform.position = grid.LocalToWorld(grid.CellToLocalInterpolated(new Vector3Int(position.x, position.y, Z) + new Vector3(0.0f, 1f, 0f)));
            }
        }

        private void ClearSceneCell(GridLayout grid, Transform parent, Vector3Int position)
        {
            Transform erased = GetObjectInCell(grid, parent, new Vector3Int(position.x, position.y, Z));
            if (erased != null)
                Undo.DestroyObjectImmediate(erased.gameObject);
        }

        public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
        {
            if (brushTarget != null)
            {
                _prevBrushTarget = brushTarget;
            }

            brushTarget = _prevBrushTarget;

            // Don't allow editing palettes (??)
            if (brushTarget.layer == 31)
                return;

            ClearSceneCell(gridLayout, brushTarget != null ? brushTarget.transform : null, position);
        }

        public override void BoxErase(GridLayout gridLayout, GameObject brushTarget, BoundsInt position)
        {
            if (brushTarget != null)
            {
                _prevBrushTarget = brushTarget;
            }

            brushTarget = _prevBrushTarget;

            // Don't allow editing palettes (??)
            if (brushTarget.layer == 31)
                return;

            foreach(Vector3Int location in position.allPositionsWithin)
            {
                ClearSceneCell(gridLayout, brushTarget != null ? brushTarget.transform : null, location);
            }
        }

        public override void Pick(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, Vector3Int pickStart)
        {
            if (brushTarget != null)
            {
                _prevBrushTarget = brushTarget;
            }

            brushTarget = _prevBrushTarget;
            // Don't allow editing palettes (??)
            if (brushTarget.layer == 31)
                return;

            foreach (Vector3Int pos in position.allPositionsWithin)
            {
                Vector3Int brushPosition = new Vector3Int(pos.x - position.x, pos.y - position.y, 0);
                PickCell(pos, brushPosition, gridLayout, brushTarget != null ? brushTarget.transform : null);
            }
        }

        private void PickCell(Vector3Int position, Vector3Int brushPosition, GridLayout grid, Transform parent)
        {
            Vector3 cellCenter = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
            Transform go = GetObjectInCell(grid, parent, position);

            if (go != null)
            {
                Object prefab = PrefabUtility.GetCorrespondingObjectFromSource(go);
                if (prefab is Transform prefabGo)
                {
                    bool found = false;
                    for (int i = 0; i < Prefabs.Count; i ++)
                    {
                        if (Prefabs[i] == prefabGo.gameObject)
                        {
                            PrefabToPrint = i;
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        Prefabs.Add(prefabGo.gameObject);
                    }
                }
            }
        }

        private static Transform GetObjectInCell(GridLayout grid, Transform parent, Vector3Int position)
        {
            int childCount = parent.childCount;
            Vector3 min = grid.LocalToWorld(grid.CellToLocalInterpolated(position));
            Vector3 max = grid.LocalToWorld(grid.CellToLocalInterpolated(position + new Vector3Int(1,1,0)));
            Bounds bounds = new Bounds((max + min) * 0.5f, (max - min) * 0.999f);

            for (int i = 0; i < childCount; i++)
            {
                Transform child = parent.GetChild(i);
                if (bounds.Contains(child.position + new Vector3(0.5f, -0.5f, 0.0f)))
                    return child;
            }
            return null;
        }

        private static float GetPerlinValue(Vector3Int position, float scale, float offset)
        {
            return Mathf.PerlinNoise((position.x + offset) * scale, (position.y + offset) * scale);
        }
    }

    [CustomEditor(typeof(PrefabBrush))]
    public class PrefabBrushEditor : UnityEditor.Tilemaps.GridBrushEditor
    {
        private PrefabBrush _prefabBrush { get { return target as PrefabBrush; } }

        private SerializedProperty _prefabs;
        private SerializedObject _serializedObject;

        protected override void OnEnable()
        {
            base.OnEnable();
            _serializedObject = new SerializedObject(target);
            _prefabs = _serializedObject.FindProperty("Prefabs");
        }

        public override void OnPaintInspectorGUI()
        {
            _serializedObject.UpdateIfRequiredOrScript();
            _prefabBrush.PerlinScale = EditorGUILayout.Slider("Perlin Scale", _prefabBrush.PerlinScale, 0.001f, 0.999f);
            _prefabBrush.Z = EditorGUILayout.IntField("Position Z", _prefabBrush.Z);
            EditorGUILayout.PropertyField(_prefabs, true);

            _prefabBrush.RandomPrefab = EditorGUILayout.Toggle("Random Prefab", _prefabBrush.RandomPrefab);
            _prefabBrush.PrefabToPrint = EditorGUILayout.IntField("Prefab to Print", _prefabBrush.PrefabToPrint);

            _serializedObject.ApplyModifiedPropertiesWithoutUndo();

        }
    }
}